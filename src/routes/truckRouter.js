const express = require('express')
const router = express.Router()
const {postNewTruck, getUsersTruck,getUsersTruckById,putUsersTruckById,deleteUsersTruckById,patchAssignUserToTruck} = require('../services/truckService')
const {authMiddleware} = require('../middleware/authMiddleware')


router.post('/', authMiddleware, postNewTruck)

router.get('/:id', authMiddleware, getUsersTruckById)

router.get('/', authMiddleware, getUsersTruck)

router.put('/:id',authMiddleware,putUsersTruckById)

router.delete('/:id',authMiddleware,deleteUsersTruckById)

router.post('/:id/assign',authMiddleware,patchAssignUserToTruck)


module.exports = {
    truckRouter:router,
}
