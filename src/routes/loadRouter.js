const express = require('express')
const router = express.Router()
const {postLoads, deleteUsersLoadById,patchLoadsToActive, getUsersLoads, putLoadById,getUsersLoadById,getActiveLoads,postNewLoad,getShippingInfo} = require('../services/loadService')
const {authMiddleware} = require('../middleware/authMiddleware')

router.post('/',authMiddleware,postLoads)

router.patch('/active/state',authMiddleware,patchLoadsToActive)

router.delete('/:id',authMiddleware,deleteUsersLoadById)

router.get('/active',authMiddleware,getActiveLoads)

router.get('/',authMiddleware,getUsersLoads)

router.put('/:id',authMiddleware,putLoadById)

router.get('/:id',authMiddleware,getUsersLoadById)


router.post('/:id/post',authMiddleware,postNewLoad)

router.get('/:id/shipping_info',authMiddleware,getShippingInfo)

module.exports = {
    loadRouter:router,
}
