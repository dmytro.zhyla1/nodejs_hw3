const express = require('express')
const router = express.Router()
const {registerUser,loginUser,getUserInfo,deleteUser,pathUserPassword} = require('../services/usersService')
const {authMiddleware} = require('../middleware/authMiddleware')

router.post('/auth/register', registerUser);

router.post('/auth/login', loginUser);

router.delete('/users/me', authMiddleware, deleteUser);

router.get('/users/me',authMiddleware, getUserInfo);

router.patch('/users/me/password', authMiddleware, pathUserPassword)


module.exports = {
    usersRouter: router,
}
