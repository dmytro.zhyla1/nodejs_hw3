const {Load} = require('../models/Load')
const {Truck} = require('../models/Truck')
const mongoose = require("mongoose");

const postLoads = (req, res, next) => {
    if (req.user.role !== 'SHIPPER') {
        res.status(400).send({
            message: "You a not a Shipper"
        })
        return
    }
    const {name, payload, pickup_address, delivery_address} = req.body;
    let Ox = req.body.dimensions.width
    let Oy = req.body.dimensions.height
    let Oz = req.body.dimensions.width
    let message = `Load assigned to driver with id ###`
    const load = new Load({
        created_by: req.user.userId,
        assigned_to: req.user.userId,
        status: "NEW",
        state: "En route to Pick Up",
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions: {width: Ox, length: Oy, height: Oz},
        logs: [{
            message: message,
            time: Date(),

        }]
    })
    load.save()
        .then(() => res.json({message: 'Load created successfully'}))
        .catch(err => {
            next(err);
        });
}

const putLoadById = async (req, res) => {
    if (req.user.role !== 'SHIPPER') {
        res.status(400).send({
            message: "You a not a Shipper"
        })
        return
    }
    const load = await Load.findById(req.params.id);
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
    if (name) load.name = name;
    if (payload) load.payload = payload;
    if (pickup_address) load.pickup_address = pickup_address;
    if (delivery_address) load.delivery_address = delivery_address;
    if (dimensions) load.dimensions = dimensions;

    return load.save().then((saved) => {
        if (req.user.userId === saved.created_by.toString()) {
            res.status(200).send({
                "message": "Load details changed successfully"
            })
            return
        } else {
            res.status(400).send({message: "wrong id"})
        }
    });
}

function getUsersLoads(req, res) {
    if (req.user.role !== 'SHIPPER') {
        res.status(400).send({
            message: "You a not a Shipper"
        })
        return
    }
    return Load.find().then((result) => {
        let arr = []
        result.forEach((element) => {
            if (req.user.userId === element.created_by.toString()) {
                arr.push(element)
            }
        })
        res.status(200).send({
            "loads": arr
        })
    });
}

async function patchLoadsToActive(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a Driver"
        })
        return
    }
    const load = await Load.findById(req.params.id);
    load.state = 'En route to Delivery'
    return load.save().then(() => {
        res.status(200).send({
            "message": "Load state changed to 'En route to Delivery'"
        })
        return
    });
}

function deleteUsersLoadById(req, res) {
    if (req.user.role !== 'SHIPPER') {
        res.status(400).send({
            message: "You a not a Shipper"
        })
        return
    }
    return Load.findByIdAndDelete(req.params.id)
        .then((load) => {
            if (req.user.userId === load.created_by.toString()) {
                res.send({
                    "message": "Load deleted successfully"
                });
                return;
            } else {
                res.status(400).send({message: "wrong id"})
            }
        })
}

function getUsersLoadById(req, res) {
    if ((!mongoose.Types.ObjectId.isValid(req.params.id))) {
        res.status(400).send({message: "wrong id"})
        return
    }
    return Load.findById(req.params.id)
        .then((load) => {
            res.status(200).send({
                "load": load,
            })
        })
}

function getActiveLoads(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a Driver"
        })
        return
    }
    return Load.find().then((result) => {
        let arr = []
        result.forEach((element) => {
            if (result.logs[0] !== "Load assigned to driver with id ###") {
                arr.push(element)
            }
        })
        res.status(200).send({
            "loads": arr
        })
    })
}

async function postNewLoad(req, res) {
    let findedTruck = {}
    let message = `Load assigned to driver with id ###`
    if (req.user.role !== 'SHIPPER') {
        res.status(400).send({
            message: "You a not a Shipper"
        })
        return
    }
    if ((!mongoose.Types.ObjectId.isValid(req.params.id))) {
        res.status(400).send({message: "wrong id"})
        return
    }
    const load = await Load.findById(req.params.id);
       await Load.findById(req.params.id)
        .then(async () => {
           await Truck.find().then((truckResult) => {
                for (let i = 0; i < truckResult.length; i++) {
                    if (truckResult[i].status === 'IS') {
                        truckResult[i].status = 'OL'
                        findedTruck = truckResult[i]
                        message = `Load assigned to driver with id ${findedTruck._id}`
                        findedTruck.save()
                        req.body.Truck = findedTruck
                        return
                    }
                }
            })
            res.status(200).send({
                "message": "Load posted successfully",
                "driver_found": true
            })
        })
    load.logs[0].message = message
    load.markModified('logs');
    load.save().then()
}

function getShippingInfo(req, res) {
    if ((!mongoose.Types.ObjectId.isValid(req.params.id))) {
        res.status(400).send({message: "wrong id"})
        return
    }
    return Load.findById(req.params.id)
        .then((load) => {
            res.status(200).send({
                "load": load,
                "truck":req.body.Truck
            })
        })
}

module.exports = {
    postLoads,
    patchLoadsToActive,
    deleteUsersLoadById,
    getUsersLoads,
    putLoadById,
    getUsersLoadById,
    getActiveLoads,
    postNewLoad,
    getShippingInfo

}
