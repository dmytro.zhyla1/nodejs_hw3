const {User} = require("../models/Users.js");
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
    const {email, password, role} = req.body;
    if (role === undefined || email === undefined || password === undefined) {
        res.status(400).json({message: 'please check your request'})
        return
    }
    const user = new User({
        email,
        password: await bcryptjs.hash(password, 10),
        role
    });
    user.save()
        .then(() => res.json({message: 'Success!'}))
        .catch(err => {
            next(err);
        });
}

const loginUser = async (req, res, next) => {
    const user = await User.findOne({email: req.body.email});
    let userRole = user.role
    if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
        const payload = {email: user.email, userId: user._id, role: user.role}
        const jwtToken = jwt.sign(payload, 'secret-jwt-key');
        return res.json({
            message: "Success",
            jwt_token: jwtToken
        });
        return res.status(400).json({'message': 'Not authorized'});
    }
}

const deleteUser = async (req, res) => {
    User.findByIdAndDelete(req.user.userId)
        .then((user) => {
            res.status(200).send({
                "message": "Profile deleted successfully"
            })
        })
}

async function getUserInfo(req, res) {
    User.findById(req.user.userId)
        .then((user) => {
            res.status(200).send({
                "user": {
                    "_id": user._id,
                    "role": user.role,
                    "email": user.email,
                    "created_date": user.created_date,
                }
            })
        })
}

async function pathUserPassword(req, res) {
    const user = await User.findById(req.user.userId);
    if ((String(req.body.oldPassword), String(user.password))) {
        user.password = req.body.newPassword
        user.password = await bcryptjs.hash(user.password, 10)
        user.save()
        res.status(200).send({
            "message": "Success"
        })
    } else {
        res.status(400).send({
            "message": "Error"
        })
    }
}

module.exports = {
    registerUser,
    loginUser,
    deleteUser,
    getUserInfo,
    pathUserPassword
};
