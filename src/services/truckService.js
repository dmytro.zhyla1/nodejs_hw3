const {Truck} = require('../models/Truck')
const mongoose = require("mongoose");

const postNewTruck = (req, res, next) => {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a driver"
        })
        return
    }
    const {type} = req.body;
    const truck = new Truck({
        created_by: req.user.userId,
        type: type,
    })
    truck.save()
        .then(() => res.json({message: 'Truck created successfully'}))
        .catch(err => {
            next(err);
        });
}

function getUsersTruck(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a driver"
        })
        return
    }
    return Truck.find().then((result) => {
        let arr = []
        result.forEach((element) => {
            if (req.user.userId === element.created_by.toString()) {
                arr.push(element)
            }
        })
        res.status(200).send({
            "trucks": arr
        })
    });
}

function getUsersTruckById(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a driver"
        })
        return
    }
    if ((!mongoose.Types.ObjectId.isValid(req.params.id))) {
        res.status(400).send({message: "wrong id"})
        return
    }
    return Truck.findById(req.params.id)
        .then((truck) => {
            res.status(200).send({
                "truck": truck,
            })
        })
}

async function putUsersTruckById(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a driver"
        })
        return
    }
    const truck = await Truck.findById(req.params.id);
    const {type} = req.body;
    if (type) truck.type = type;
    return truck.save().then((saved) => {
        if (req.user.userId === saved.created_by.toString()) {
            res.status(200).send({
                "message": "Truck details changed successfully"
            })
            return
        } else {
            res.status(400).send({message: "wrong id"})
        }
    });
}

function deleteUsersTruckById(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a driver"
        })
        return
    }
    return Truck.findByIdAndDelete(req.params.id)
        .then((truck) => {
            if (req.user.userId === truck.created_by.toString()) {
                res.send({
                    "message": "Truck deleted successfully"
                });
                return;
            } else {
                res.status(400).send({message: "wrong id"})
            }
        })
}

async function patchAssignUserToTruck(req, res) {
    if (req.user.role !== 'DRIVER') {
        res.status(400).send({
            message: "You a not a driver"
        })
        return
    }
    console.log(req.params.id)
    const truck = await Truck.findById(req.params.id);
    truck.assigned_to = req.params.id
    return truck.save().then(() => {
        res.status(200).send({
            "message": "Truck assigned successfully"
        })
        return
    });
}

module.exports = {
    postNewTruck,
    getUsersTruck,
    getUsersTruckById,
    patchAssignUserToTruck,
    putUsersTruckById,
    deleteUsersTruckById,
}
