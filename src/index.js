const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')

const app = express()

mongoose.connect('mongodb+srv://dima:33333@hw3.xrh6heq.mongodb.net/?retryWrites=true&w=majority')

const {usersRouter} = require('./routes/usersRouter')
const {truckRouter} = require('./routes/truckRouter')
const {loadRouter} = require('./routes/loadRouter')

app.use(express.json())
app.use(morgan('tiny'))

app.use('/api', usersRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads',loadRouter)

const start = async () => {
    try {
        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
};

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
    console.error(err);
    res.status(500).send({message: 'Server error'});
}
