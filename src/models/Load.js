const mongoose = require('mongoose');

const schema = mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    state: {
        type: String,
        default:"En route to Pick Up",
        required: true,
    },
    status: {
        type: String,
        default:"NEW",
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    pickup_address: {
        type: String,
        required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        width: {type: Number},
        length: {type: Number},
        height: {type: Number}
    },
    logs: {
        type: Array,

    },
}, {timestamps: {createdAt: "created_date"},updatedAt:false});

let Load = mongoose.model('Load', schema)

module.exports = {
    Load,
};
