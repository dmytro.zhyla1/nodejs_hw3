const mongoose = require('mongoose');

const schema = mongoose.Schema({
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default:null,
    },
    type: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
        default: 'IS'
    },
},{timestamps:{createdAt: "created_date"},updatedAt:false});

let Truck = mongoose.model('Truck', schema)

module.exports = {
    Truck,
};
