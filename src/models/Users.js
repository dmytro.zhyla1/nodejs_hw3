const mongoose = require('mongoose');

const schema = mongoose.Schema({
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
},{timestamps:{createdAt: "created_date",updatedAt:false}});

let User = mongoose.model('User', schema)

module.exports = {
  User,
};
